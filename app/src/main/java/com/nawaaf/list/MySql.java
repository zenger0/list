package com.nawaaf.list;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class MySql extends SQLiteOpenHelper {

    SQLiteDatabase sqLiteDatabase;

    private static final String DB_NAME = "MySqlDatabase";

    private static final int VERSION_NUMBER = 1;

    public static final String STUDENT_TABLE = "student";
    public static final String IDSTUDENT = "idsrudent";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";

    private static final String STUDENT_CREATE = "create table " + STUDENT_TABLE + " ("
            + IDSTUDENT+" text PRIMARY KEY ,"
            + NAME     +"text not null,"
            + PASSWORD    +"text not null)";

    //----------------------------------------
    public static final String HALLS_TABLE = "halls";
    public static final String HIDSTUDENT ="hidsrudent";
    public static final String DAY = "day";
    public static final String COURS = "cours";
    public static final String Start_Lecture = "start_Lecture";
    public static final String End_Lecture = "end_Lecture";



    private static final String LECTURE_CREEAT = "create table " + HALLS_TABLE + " ("
            + HIDSTUDENT+"text  PRIMARY KEY,"
            + DAY + " text  not null,"
            + Start_Lecture + " text not  null,"
            + End_Lecture + " text  not null,"
            + COURS + " text  not null )";


    public MySql(Context context) {
        super(context, DB_NAME, null, VERSION_NUMBER);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(STUDENT_CREATE);
        sqLiteDatabase.execSQL(LECTURE_CREEAT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST" + STUDENT_TABLE);
        onCreate(sqLiteDatabase);
    }

}