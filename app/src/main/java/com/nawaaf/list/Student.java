package com.nawaaf.list;

/**
 * Created by nawaf on 2/18/16.
 */

public class Student  {

    private int id;

    private String idstudent;
    private String name;
    private String password;
    private String start_lecture;
    private String end_lecture;
    private String day;
    private String cours;



    public String getIDstudent() {
        return idstudent;
    }

    public void setIDstudent(String  idstudent) {
        this.idstudent = idstudent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password ;
    }

    //-----------------Lecture---------------------------

    public String getStart_lecture() {
        return start_lecture;
    }

    public void setStart_lecture (String start_lecture) {
        this.start_lecture = start_lecture;
    }

    public String getEnd_lecture() {
        return end_lecture;
    }

    public void setEnd_lecture(String end_lecture) {
        this.end_lecture = end_lecture;
    }

    public String getDay() { return day; }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCours() {
        return cours;
    }

    public void setCours(String cours) {
        this.cours = cours;
    }


    //----------------Lecture----------------------------


}