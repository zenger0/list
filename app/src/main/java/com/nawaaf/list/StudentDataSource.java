package com.nawaaf.list;

/**
 * Created by nawaf on 2/18/16.
 */




import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.Editable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class StudentDataSource {

    MySql mySql;
    SQLiteDatabase sqLiteDatabase;
     String[] allColumes=new String[]{ MySql.IDSTUDENT,MySql.NAME,MySql.PASSWORD ,MySql.DAY
             ,MySql.Start_Lecture,MySql.End_Lecture,MySql.COURS };

    public StudentDataSource(Context context){
        mySql=new MySql(context);

    }

    public void open(){
        try {
            sqLiteDatabase = mySql.getWritableDatabase();
        }catch (Exception e){
            Log.d("ERROR",e.getMessage());
        }
    }

    public void close(){
        sqLiteDatabase.close();
    }


    public void createStudent(String IDstudent, String name, String password   ){
        ContentValues list=new ContentValues();
        list.put(MySql.IDSTUDENT, IDstudent);
        list.put(MySql.NAME,name);
        list.put(MySql.PASSWORD,password);




        sqLiteDatabase.insert(MySql.STUDENT_TABLE, null, list);

    }


    public void createLecture(String IDstudent ,String start_lecture ,String end_lecture ,String day ,String cours  ){
        ContentValues list=new ContentValues();
        list.put(MySql.IDSTUDENT,IDstudent);
        list.put(MySql.Start_Lecture,start_lecture);
        list.put(MySql.End_Lecture,end_lecture);
        list.put(MySql.COURS,cours);
        list.put(MySql.DAY,day);



        sqLiteDatabase.insert(MySql.HALLS_TABLE, null, list);

    }


    public Student getStudent(String idsrudent){
        Student student=new Student();
        Cursor cursor=sqLiteDatabase.rawQuery("SELECT * FROM "+MySql.STUDENT_TABLE+" WHERE "+MySql.IDSTUDENT+" = ?",new String[]{idsrudent+""});
        if(cursor == null )
            return student;

        if(cursor.getCount()>0){
            cursor.moveToFirst();
            student.setIDstudent(cursor.getString(0));
            student.setName(cursor.getString(1));
            student.setPassword(cursor.getString(2));
            //student.setStart_lecture(cursor.getString(3));
           // student.setEnd_lecture(cursor.getString(4));
           // student.setDay(cursor.getString(5));
            //student.setCours(cursor.getString(6));
            cursor.close();
        }

        return student;
    }
    public void updateStudent(String IDstudent,String name,String password ,String start_lecture ,String end_lecture ,String day ,String cours ){
        ContentValues list=new ContentValues();
        list.put(MySql.IDSTUDENT,IDstudent);
        list.put(MySql.NAME,name);
        list.put(MySql.PASSWORD,password);
        list.put(MySql.Start_Lecture,start_lecture);
        list.put(MySql.End_Lecture,end_lecture);
        list.put(MySql.DAY,day);
        list.put(MySql.COURS,cours);

        sqLiteDatabase.update(MySql.STUDENT_TABLE, list, MySql.IDSTUDENT + " = " + IDstudent, null);


    }

    public void deletStudent(String idstudent){
        sqLiteDatabase.delete(MySql.STUDENT_TABLE, MySql.IDSTUDENT + " = " + idstudent, null);

    }





    public List<Student> getAllStudents(){
        List<Student> studentList=new ArrayList<Student>();

        Cursor cursor=sqLiteDatabase.query(MySql.STUDENT_TABLE, allColumes, null, null,null,null,null );
         cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Student student=new Student();
            student.setIDstudent(cursor.getString(0));
            student.setName(cursor.getString(1));
            student.setPassword(cursor.getString(2));
            studentList.add(student);
            cursor.moveToNext();

        }
        cursor.close();
        return studentList;

    }


    public boolean isValidStudent(Editable text, Editable text1) {
        Cursor student = getStudentByNameAndPass(text.toString(), text1.toString());
        if(student == null)
            return false;
        else if(student.getCount() > 1)
            return false;
        return true;
    }

    private Cursor getStudentByNameAndPass(String name, String pass) {
        SQLiteDatabase db = mySql.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                mySql.IDSTUDENT,
                mySql.NAME,
                mySql.PASSWORD
        };

        String[] selectionArgs = {name};
        Cursor c = db.query(
                mySql.STUDENT_TABLE,  // The table to query
                projection,                               // The columns to return
                mySql.NAME,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        return c;
    }
}