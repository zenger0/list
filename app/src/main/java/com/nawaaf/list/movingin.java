package com.nawaaf.list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class movingin extends AppCompatActivity {

    Button go ,Choosing  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movingin);

        go =(Button)findViewById(R.id.buttonGo);

        Choosing =(Button)findViewById(R.id.buttonChoosing);


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(movingin.this, namberLab.class));
            }
        });

        Choosing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(movingin.this, writingsLab.class));
            }
        });
    }
}
